# MicroService

> Um pequeno framework para microserviços utilizando NodeJS.

Este é um framework barebones, com funcionalidades de models, controllers, migrations, seeders, middlewares e rotas, e nada mais. Várias de suas funcionalidades foram criadas seguindo a filosofia CoC (Convention over Configuration).

Suas principais funcionalidades incluem o uso do ORM Sequelize, com suporte à migrations e seeders; utilização de `generators` e `yield` para evitar o "callback hell"; construído sobre o ExpressJS; e a sua simplicidade de uso.

## Instalação

Primeiro, certifique-se de utilizar o registro privado `http://npm.fundacred.org.br:7777` através de um arquivo de configuração `.npmrc` ou pelo parâmetro do NPM `--registry`.

Em seguida, instale através do `npm`:

```
npm install --save micro-service
```

Ou com o parâmetro `--registry`:

```
npm install --registry="http://npm.fundacred.org.br:7777" --save micro-service
```

Por fim, precisamos da CLI do Sequelize:

```
npm install -g sequelize-cli
```

## Utilização

Basta requerer o pacote e executá-lo:

```javascript
require('micro-service')();
```

## Estrutura de projeto

Seguindo a filosofia CoC, este framework possui algumas convenções sobre a estrutura de diretório e arquivos de um serviço, conforme visto a seguir.

```
meu-projeto
├── config
│   ├── routes.json
│   └── [config.json]
├── controllers
│   ├── [controller.js]
│   └── ...
├── environments
│   ├── [ambiente]
│   │   ├── [config.json]
│   │   └── ...
│   ├── ...
│   │   └── ...
├── libs
│   ├── [lib.js]
│   └── ...
├── middlewares
│   ├── [middleware.js]
│   └── ...
├── migrations
├── models
│   ├── [model.js]
│   └── ...
├── seeders
│   ├── [seeder.js]
│   └── ...
├── templates
│   ├── [template.txt]
│   ├── [template.html]
│   └── ...
├── index.js
└── package.json
```

## Convenções

Antes de qualquer código, algumas convenções devem ser estabelecidas para um melhor proveito do framework.

### Nomes de arquivos

Todos os arquivos devem seguir o padrão `dash-case` de nomenclatura. Por exemplo:

```
animals-controller.js
```

Esta nomenclatura é importante e será vista em detalhes nas demais seções adiante.

### Generators

Todas as funções que tratam requests, como middlewares e controllers, devem ser declaradas como generators, utilizando `*` na declaração da função.

## Configurações

Todo serviço deve ser facilmente configurável, e para isto temos um diretório chamado `config`. Todo e qualquer arquivo JSON localizado neste diretório será automaticamente carregado durante a inicialização do serviço e convertido para um Object, sendo armazenado na variável global `config` com o nome do arquivo convertido para `camelCase`.

Como exemplo, vamos criar um arquivo chamado `o-auth-keys.json` no diretório `config`, com este conteúdo:

```json
{
	"key": "0123456789ABCDEF",
	"secret: "foobar"
}
```

Em nosso código, quando for necessário acessar a variável `secret` deste JSON, basta utilizarmos o seguinte código:

```javascript
var myVar = global.config.oAuthKeys.secret;
```

Denotando, assim, a importância das convenções e nomenclatura.

## Ambientes

Configurações separadas por ambiente são suportadas através do diretório `environments`. Seu funcionamento é idêntico ao das configurações, mas as configurações são agrupadas em subdiretórios dentro de `environments`, um para cada ambiente. Exemplo:

```
meu-projeto
└── environments
    ├── development
    │   └── [config.json]
    │   └── ...
    ├── test
    │   └── [config.json]
    │   └── ...
    └── production
        └── [config.json]
        └── ...
```

Todas as configurações de ambiente são tratadas e armazenadas conforme as configurações globais, mas dentro de uma variávei `environment`.

Para acessar uma configuração armazenda em `environments/development/cookies.json`, devemos proceder conforme a seguir:

```javascript
var configs = global.config.environment.cookies.foo;
```

## Rotas

Todas as rotas são definidas no arquivo de configuração `config/routes.json`. Este arquivo é um JSON simples contendo um Array de rotas.

Cada rota é declarada como um Array contendo, nesta ordem, o método HTTP, a URL e a ação que será executada no formato `Controller.nomeAcao`.

Um exemplo de uma rota para listar todos os animais cadastrados:

```json
[
	[ "get", "/animals", "AnimalsController.list" ]
]
```

Rotas também suportam parâmetros dinâmicos, bastando preceder seu nome por `:`. Neste caso, podemos criar uma rota para detalhar um animal específico:

```json
[
	[ "get", "/animals/:id", "AnimalsController.show" ]
]
```

## Middlewares

Middlewares são armazenados no diretório `middlewares` como módulos do Node. Cada middleware é definido como um Object, contendo as propriedades `priority`, que é um número, e `middleware`, que é a função do middleware em si. Exemplo:

```javascript
// Validation middleware.

module.exports = {
	priority: 1,

	middleware: function* (req, res, next) {
		if (req.headers.authorization == 'abc123') {
			return next();
		} else {
			return res.status(403).send();
		}
	}
};
```

O valor de `priority` define a ordem em que os middlewares serão executados, sendo ordenados do menor para o maior.

## Models

### Configurando a conexão com o banco de dados

Este framework utiliza o ORM Sequelize como camada de comunicação com o banco de dados. Para utilizá-los, é necessário um arquivo de configuração em `config/database.json`. É uma configuração simples, dividida por ambientes, e cada ambiente possui sua configuração de banco, seguindo a documentação do Sequelize.

Exemplo:

```json
{
	"development": {
		"dialect": "postgres",
		"host": "localhost",
		"port": 5432,
		"username": "username",
		"password": "password"
	},

	"production": {
		"dialect": "postgres",
		"host": "localhost",
		"port": 5432,
		"username": "username",
		"password": "password"
	}
}
```

Após configurar o banco de dados, podemos criar models dentro do diretório `models`, seguindo as convenções de nomenclatura para os nomes de arquivo.

### Declaração de models

Todos os models são carregados e armazenados na variável `global`, podendo serem acessados de qualquer lugar do serviço. O nome do model é deduzido do nome do arquivo, sendo convertido para `PascalCase`. Se o nosso model estiver armazenado em `meu-model.js`, ele será carregado em `MeuModel`.

Cada model é declarado como um módulo do Node em forma de Object, possuindo três propriedades: `attributes`, `associations` e `options`.

Exemplo de declaração de um model:

```javascript
{
	attributes: {
		// ...
	},

	associations: [
		// ...
	],

	options: {
		// ...
	}
}
```

#### Attributes

Em `attributes` é onde definimos quais propriedades o model possuirá. O nome das propriedades segue a convenção de `camelCase`. Esta propriedade é declarada na forma de um Object, e cada propriedade por sua vez também é um Object, conforme o exemplo a seguir:

```javascript
{
	attributes: {
		name: {
			type: Sequelize.STRING,
			allowNull: false
		},

		email: {
			type: Sequelize.STRING
		}
	}
}
```

As opções de cada propriedade são as mesmas descritas pelo Sequelize, e estas podem ser consultadas na [documentação](http://docs.sequelizejs.com/en/latest/docs/models-definition).

#### Associations

A propriedade `associations` é declarada como um Array, aonde cada item descreve uma associação. Cada associação também é declarada como um Array contendo dois itens obrigatórios e um opcional, nesta ordem: tipo da associação, nome do model e opções da associação. Exemplo:

```javascript
{
	associations: [
		[ 'hasOne', 'Job' ],
		[ 'hasMany', 'Animal', { as: 'pets' } ],
		[ 'belongsTo', 'User' ],
		[ 'belongsToMany', 'Tag', { as: 'tags', through: 'my_tags' } ]
	]
}
```

#### Options

Esta propriedade é declarada como um Object, sendo responsável por opções do Sequelize como `paranoid`, `timestamps`, etc. Mais detalhes podem ser vistos [aqui](http://docs.sequelizejs.com/en/latest/docs/associations).

#### Models sem ID

Em alguns casos é necessário criar models que não possuem um ID, e para isto basta utilizar a opção `noId: true` dentro de `options`.

## Migrations e seeders

### Migrations

Uma boa prática quando se trabalha com outras pessoas em um mesmo projeto é que nunca tocamos diretamente no banco de dados. Para criar novas tabelas, adicionar colunas ou qualquer outra alteração, utilizamos o conceito de "migrations".

Migrations são arquivos com código JavaScript com dois métodos: `up` e `down`, e cada um, respectivamente, executa ações de progredir e regredir. No método `up` é escrito código que será executado quando a migration for executada de forma normal. Já o método `down` possui código que é executado quando desejamos desfazer as ações de uma migration.

Para criar uma migration, utiliza-se o comando do Sequelize:

```
sequelize migration:create --name "nome-da-migration"
```

Para executar todas as migrations pendentes:

```
sequelize db:migrate
```

O Sequelize guarda um histórico de quais migrations foram executadas, evitando que operações sejam repetidas.

Caso seja necessário desfazer a última migration:

```
sequelize db:migrate:undo
```

> :warning: _**Cuidado!** Migrations que executam ações destrutivas, como `removeColumn` ou `dropTable`, não são capazes de desfazer a destruição dos dados. O método `down` serve para, nestes casos, reconstruir a estrutura, mas não a reconstituição dos dados. Use com cautela._

### Seeders

Seeders, ao contrário das migrations, não manipulam a estrutura do banco de dados, mas sim dos dados contidos neste. Seu ponto forte se encontra na inserção de dados padrão para a primeira execução de um serviço, como por exemplo, a criação de um usuário administrador.

Para criar um seeder, utiliza-se o comando do Sequelize:

```
sequelize seeder:create --name "nome-do-seeder"
```

E para executar os seeders, utiliza-se o comando do Sequelize:

```
sequelize db:seed:all
```

Para mais informações sobre migrations e seeders, consulte a documentação [aqui](http://docs.sequelizejs.com/en/latest/docs/migrations).

## Libs

O diretório `libs` armazena módulos que não são controllers, models ou middlewares. Podem ser vistos como bibliotecas que realizam alguma atividade que não o serviço em si.

Não existem muitas restrições aqui. Novamente, atenção para a convenção de nome de arquivo. Todas as libs serão carregadas na variável `global` com seu nome convertido para `PascalCase`, assim como os models.

## Templates

Templates são arquivos de texto em qualquer formato: texto puro, HTML, XML, JSON, etc. Todos os arquivos no diretório `templates` são carregados e armazenados em `global.templates`, com seu nome convertido para `camelCase`. No caso de um template com nome `welcome-email.html`, este pode ser acessado da seguinte maneira:

```javascript
var template = global.templates.welcomeEmail;
```

A utilização do template é livre e deve ser feita pelo desenvolvedor. No entanto, recomendamos o [Mustache](https://github.com/janl/mustache.js), de fácil utilização e leitura.
