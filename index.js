module.exports = function () {
  var banner = `
█████████████████████████████████████████████████████████████████████████
█       █ █████ █  ████ █     ███       ███     █      ██       █     ███
█ ███████ █████ █ █ ███ █ ████ ██ █████ ██ ██████ █████ █ ███████ ████ ██
█     ███ █████ █ ██ ██ █ █████ █       █ ███████ █████ █    ████ █████ █
█ ███████ █████ █ ███ █ █ ████ ██ █████ ██ ██████      ██ ███████ ████ ██
█ ███████       █ ████  █     ███ █████ ███     █ █████ █       █     ███
█████████████████████████████████████████████████████████████████████████
`;
  var fs = require('fs');
  var path = require('path');
  var process = require('process');

  global.Express = require('express');
  global.Sequelize = require('sequelize');
  global.cwd = require('process').cwd();
  
  var pkg = JSON.parse(fs.readFileSync(`${cwd}/package.json`, { encoding: 'utf8' }));
  var server;

  var dashCaseToUpperCamelCase = function (str) {
    return str.toLowerCase().split('-').map(function (el, i) { return el[0].toUpperCase() + el.slice(1); }).join('');
  };

  var dashCaseToLowerCamelCase = function (str) {
    return str.toLowerCase().split('-').map(function (el, i) { return i == 0 ? el : el[0].toUpperCase() + el.slice(1); }).join('');
  };

  var dashCaseToUnderscoreCase = function (str) {
    return str.toLowerCase().split('-').join('_');
  };

  // Creates functions to enable us to use `yield` inside actions, as well as
  // transform common node-style callbacks into Promises.
  var prepareRunnerAndSwearer = function () {
    global.swear = function () {
      var fn = arguments[0].f || arguments[0];
      var t = arguments[0].t;
      var args = Array.prototype.slice.apply(arguments, [1]);

      return new Promise(function (resolve, reject) {
        var cb = function (err, a, b, c, d, e, f) {
          if (err) {
            reject(err);
          } else {
            resolve([a, b, c, d, e, f]);
          }
        };

        args.push(cb);

        fn.apply(t, args);
      });
    };

    global.run = function (gen, args) {
      return new Promise(function (resolve, reject) {
        gen = gen.apply(null, args);

        function onFulfilled (res) {
          var ret;

          try {
            ret = gen.next(res);
          } catch (e) {
            return reject(e);
          }

          next(ret);

          return null;
        }

        function onRejected (err) {
          var ret;

          try {
            ret = gen.throw(err);
          } catch (e) {
            return reject(e);
          }

          next(ret);
        }

        function next(ret) {
          if (ret.done) {
            return resolve(ret.value);
          }

          var value = ret.value;

          if (value && ('function' == typeof value.then)) {
            return value.then(onFulfilled, onRejected);
          }

          return onRejected(new TypeError(`Expected a Promise, but got: "${String(ret.value)}"`));
        }

        onFulfilled();
      });
    };
  };

  // Load our configuration files.
  var loadConfig = function () {
    console.log('Loading configuration files...');

    global.config = {};

    fs.readdirSync(`${cwd}/config`).forEach(function (configFile) {
      var config = `${cwd}/config/${configFile}`;

      if (fs.statSync(config).isFile()) {
        if (path.extname(config) == '.json') {
          var configName = dashCaseToLowerCamelCase(path.basename(configFile, '.json'));

          global.config[configName] = JSON.parse(fs.readFileSync(`${cwd}/config/${configFile}`, { encoding: 'utf8' }));
        }
      }
    });
  };

  // Load our environment.
  var loadEnvironment = function () {
    console.log('Loading environment...');

    var args = {};

    for (var arg in process.env) {
      args[arg] = process.env[arg];
    };

    if (args.NODE_ENV == null) {
      args.NODE_ENV = 'development';
    }

    console.log(`Loading environment "${args.NODE_ENV}"...`);

    var environmentPath = `${cwd}/environments/${args.NODE_ENV}`;

    global.config.environment = {};
    global.config.environmentName = args.NODE_ENV;

    fs.readdirSync(environmentPath).forEach(function (envConfigFile) {
      var envConfig = `${environmentPath}/${envConfigFile}`;

      if (fs.statSync(envConfig).isFile()) {
        if (path.extname(envConfig) == '.json') {
          var envConfigName = dashCaseToLowerCamelCase(path.basename(envConfigFile, '.json'));

          global.config.environment[envConfigName] = JSON.parse(fs.readFileSync(`${environmentPath}/${envConfigFile}`, { encoding: 'utf8' }));
        }
      }
    });
  };

  // Load any installed dependencies.
  var loadDependencies = function () {
    console.log('Loading dependencies...');

    for (var dependency in pkg.dependencies) {
      var dependencyName = dashCaseToUpperCamelCase(dependency);

      global[dependencyName] = require(dependency);
    }

    server = Express();
  };

  // Load our libs;
  var loadLibs = function () {
    console.log('Loading libs...');

    fs.readdirSync(`${cwd}/libs`).forEach(function (libFile) {
      var libFilePath = `${cwd}/libs/${libFile}`;

      if (fs.statSync(libFilePath).isFile() && path.extname(libFilePath) == '.js') {
        var libName = dashCaseToUpperCamelCase(path.basename(libFile, '.js'));

        global[libName] = require(`${cwd}/libs/${libFile}`);
      }
    });
  };

  // Load our templates;
  var loadTemplates = function () {
    console.log('Loading templates...');

    global.templates = {};

    fs.readdirSync(`${cwd}/templates`).forEach(function (templateFile) {
      var templateFilePath = `${cwd}/templates/${templateFile}`;

      if (fs.statSync(templateFilePath).isFile() && path.extname(templateFilePath) == '.mustache') {
        var templateName = dashCaseToLowerCamelCase(path.basename(templateFile, '.mustache'));

        global.templates[templateName] = fs.readFileSync(`${cwd}/templates/${templateFile}`, { encoding: 'utf8' });
      }
    });
  };

  // Set up database.
  var setupDatabase = function () {
    console.log('Setting up database with Sequelize...');

    if (global.config.database) {
      var dbConf = global.config.database[global.config.environmentName];
      var options = {
        host: dbConf.host,
        dialect: dbConf.dialect,
        pool: dbConf.pool
      };

      if (dbConf.logging != null) {
        if (dbConf.logging != false) {
          options.logging = function (msg) { console.log('INFO:', msg); };
        } else {
          options.logging = null;
        }
      }

      global.database = new Sequelize(dbConf.database, dbConf.username, dbConf.password, options);
    }
  };

  // Load our models.
  var loadModels = function () {
    console.log('Loading models...');

    var modelsAssociations = {};

    fs.readdirSync(`${cwd}/models`).forEach(function (model) {
      if (path.extname(model) == '.js') {
        var modelName = dashCaseToUpperCamelCase(path.basename(model, '.js'));
        var modelTableName = dashCaseToUnderscoreCase(path.basename(model, '.js'));
        var modelDefinition = require(`${cwd}/models/${model}`);
        var modelOptions = modelDefinition.options || {};

        global[modelName] = module.exports = global.database.define(modelTableName, modelDefinition.attributes, modelOptions);

        if (modelDefinition.associations) {
          modelsAssociations[modelName] = modelDefinition.associations;
        }

        if (modelOptions.noId) {
          global[modelName].removeAttribute('id');
        }
      }
    });

    for (var modelName in modelsAssociations) {
      modelsAssociations[modelName].forEach(function (association) {
        var type = association[0];
        var target = association[1];
        var options = association[2];

        global[modelName][type](global[target], options);
      });
    }
  };

  // Load our controllers.
  var loadControllers = function () {
    console.log('Loading controllers...');

    fs.readdirSync(`${cwd}/controllers`).forEach(function (controller) {
      if (path.extname(controller) == '.js') {
        var controllerName = dashCaseToUpperCamelCase(path.basename(controller, '.js'));

        global[controllerName] = require(`${cwd}/controllers/${controller}`);
      }
    });
  };

  // Load any present middlewares.
  var loadMiddlewares = function () {
    console.log('Loading middlewares...');

    var middlewares = [];

    fs.readdirSync(`${cwd}/middlewares`).forEach(function (middleware) {
      if (path.extname(middleware) == '.js') {
        middlewares.push(require(`${cwd}/middlewares/${middleware}`));
      }
    });

    middlewares.sort(function (a, b) {
      return a.priority > b.priority;
    });

    middlewares.forEach(function (middleware) {
      if (Object.getPrototypeOf(middleware.middleware) === Object.getPrototypeOf(function* () {})) {
        var middlewareFunction = function (req, res, next) {
          var args = [
            req, res, next
          ];

          return run(middleware.middleware, args)
            .then(function (result) {
              return result;
            })
            .catch(function (err) {
              return res.status(500).json({
                error: {
                  name: err.name,
                  message: err.message,
                  fileName: err.fileName,
                  lineNumber: err.lineNumber,
                  stack: err.stack
                }
              });
            });
        };

        server.use(middlewareFunction);
      } else {
        server.use(middleware.middleware);
      }
    });
  };

  // Build routes.
  var buildRoutes = function () {
    console.log('Building routes...');

    global.config.routes.forEach(function (route) {
      var method = route[0];
      var path = route[1];
      var controller = route[2].split('.')[0];
      var action = route[2].split('.')[1];

      if (global[controller][action] != null) {
        var actionFunction = function (req, res, next) {
          var args = [
            req, res, next
          ];

          return run(global[controller][action], args)
            .then(function (result) {
              return result;
            })
            .catch(function (err) {
              return res.status(500).json({
                error: {
                  name: err.name,
                  message: err.message,
                  fileName: err.fileName,
                  lineNumber: err.lineNumber,
                  stack: err.stack
                }
              });
            });
        };

        server[method].apply(server, [].concat(path, actionFunction));
      } else {
        throw new ReferenceError(`Route '${method} ${path}' assigned to undefined action '${route[2]}'.`, `${cwd}/config/routes.js`, 7);
      }
    });
  };

  var startService = function () {
    server.listen(global.config.environment.server.port, function () {
      console.log(`${banner}\n\nService is up! Listening to port ${global.config.environment.server.port}.`);
    });
  };

  prepareRunnerAndSwearer();
  loadConfig();
  loadEnvironment();
  loadDependencies();
  loadLibs();
  loadTemplates();
  setupDatabase();
  loadModels();
  loadControllers();
  loadMiddlewares();
  buildRoutes();
  startService();
};
